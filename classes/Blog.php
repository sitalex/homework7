<?php
require_once 'classes/DBEntity.php';
require_once 'classes/Comments.php';


class Blog extends DBEntity
{
    protected $id      = 0;
    protected $title   = '';
    protected $intro   = '';
    protected $content = '';
    protected $entryId;

    public function __construct($title, $intro, $content, $entryId = null)
    {
        $this->title   = htmlspecialchars($title);
        $this->intro   = htmlspecialchars($intro);
        $this->content = htmlspecialchars($content);
        $this->entryId = htmlspecialchars($entryId);
    }

    static public function getBlog($id)
    {
        parent::dbs();
        try
        {
            $sql       = 'SELECT * FROM entries WHERE id=:id';
            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':id', $id);
            $statement->execute();
            $blogArr = $statement->fetchAll();
            $blogArr = $blogArr[0];
            $blogObj = new self($blogArr['title'], $blogArr['intro'], $blogArr['content'], $blogArr['entry_id']);
            $blogObj->setId($blogArr['id']);

            return $blogObj;
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    static public function delete($id)
    {
        $blog = self::getBlog($id);
        $blog->destroy();
    }

    public function destroy()
    {
        parent::dbs();
        try
        {
            $sql       = "DELETE FROM entries WHERE id=:id";
            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    static public function update($id, $title, $intro, $content)
    {
        $blog = new self($title, $intro, $content);
        $blog->setId($id);
        $blog->edit();
    }

    static public function createBlog($title, $intro, $content)
    {
        $blog = new self($title, $intro, $content);
        $blog->create();
    }

    public function create()
    {
        parent::dbs();
        try
        {
            $sql = 'INSERT INTO entries SET
            title = :title,
            intro = :intro,
            content = :content';

            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':title', $this->title);
            $statement->bindValue(':intro', $this->intro);
            $statement->bindValue(':content', $this->content);
            $statement->execute();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    static public function all()
    {
        parent::dbs();
        try
        {
            $sql      = "SELECT * FROM entries";
            $blog     = parent::$dbs->query($sql);
            $blogsArr = $blog->fetchAll();
            $blogObjs = [];
            foreach ($blogsArr as $blogArr)
            {
                $blogObj = new self($blogArr['title'], $blogArr['intro'], $blogArr['content']);
                $blogObj->setId($blogArr['id']);
                $blogObjs[] = $blogObj;
            }

            return $blogObjs;
        }
        catch (Exception $exception)
        {
            die();
        }
    }

    public function edit()
    {
        parent::dbs();
        try
        {
            $sql = 'UPDATE entries SET
            title = :title,
            intro = :intro,
            content = :content
            WHERE id=:id';

            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':title', $this->title);
            $statement->bindValue(':intro', $this->intro);
            $statement->bindValue(':content', $this->content);
            $statement->bindValue(':id', $this->id);
            $statement->execute();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getIntro()
    {
        return $this->intro;
    }

    public function setIntro($intro)
    {
        $this->intro = $intro;
    }

    public function getContent()
    {
        return $this->content;
    }

    public function setContent($content)
    {
        $this->content = $content;
    }

    public function getEntryId()
    {
        return $this->entryId;
    }

    public function setEntryId($entryId)
    {
        $this->entryId = $entryId;
    }

}