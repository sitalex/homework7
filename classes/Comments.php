<?php
require_once 'classes/DBEntity.php';

class Comments extends DBEntity
{
    protected $id;
    protected $name;
    protected $body;
    protected $entry_id;

    public function __construct($name, $body, $entry_id)
    {
        $this->name     = htmlspecialchars($name);
        $this->body     = htmlspecialchars($body);
        $this->entry_id = htmlspecialchars($entry_id);
    }

    static public function createComment($name, $body, $entry_id)
    {
        $blog = new self($name, $body, $entry_id);
        $blog->create();
    }

    public function create()
    {
        parent::dbs();
        try
        {
            $sql = 'INSERT INTO comments SET
            name = :name,
            body = :body,
            entry_id = :entry_id';

            $statement = parent::$dbs->prepare($sql);
            $statement->bindValue(':name', $this->name);
            $statement->bindValue(':body', $this->body);
            $statement->bindValue(':entry_id', $this->entry_id);
            $statement->execute();
        }
        catch (Exception $ex)
        {
            echo "Ошибка!" . $ex->getCode() . ' сообщение: ' . $ex->getMessage();
            die();
        }
    }

    static public function allComments($id)
    {
        parent::dbs();
        try
        {
            $sql         = "SELECT * FROM comments WHERE entry_id =" . $id;
            $comment     = parent::$dbs->query($sql);
            $commentsArr = $comment->fetchAll();
            $commentObjs = [];
            foreach ($commentsArr as $commentArr)
            {
                $commentObj = new Comments($commentArr['name'], $commentArr['body'], $commentArr['entry_id']);
                $commentObj->setId($commentArr['id']);
                $commentObjs[] = $commentObj;
            }

            return $commentObjs;
        }
        catch (Exception $exception)
        {
            die();
        }
    }
    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getBody()
    {
        return $this->body;
    }


    public function getEntryId()
    {
        return $this->entry_id;
    }


}