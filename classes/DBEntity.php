<?php

class DBEntity
{
    static    $dbs;

    static public function dbs()
    {
        $host       = 'localhost';
        $dbUser     = 'root';
        $dbPassword = '';
        $dbName     = 'school';
        try
        {
            self::$dbs = new PDO('mysql:host=' . $host . ';dbname=' . $dbName, $dbUser, $dbPassword);
            self::$dbs->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$dbs->exec('SET NAMES "utf8"');
        }
        catch (Exception $ex)
        {
            echo "Error connecting to db! " . $ex->getCode() . ' message: ' . $ex->getMessage();
            die();
        }
    }

}