<?php
require_once 'classes/Blog.php';
if (!empty($_GET['id']))
{
    $blog = Blog::getBlog($_GET['id']);
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="updateBlog.php" method="post">
    <input type="hidden" name="id" value="<?= $blog->getId() ?>">
    title:
    <br>
    <input type="text" name="title" value="<?= $blog->getTitle() ?>">
    <br>
    intro:
    <br>
    <input type="text" name="intro" value="<?= $blog->getIntro() ?>">
    <br>
    content:
    <br>
    <input type="text" name="content" value="<?= $blog->getContent() ?>">
    <br>
    <button>Отправить</button>
</form>
</body>
</html>
