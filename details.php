<?php
require_once 'classes/Blog.php';
require_once 'classes/Comments.php';
if (!empty($_GET['id']))
{
    $blog     = Blog::getBlog($_GET['id']);
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
title: <?= $blog->getTitle() ?>
<br>
intro: <?= $blog->getIntro() ?>
<br>
content: <?= $blog->getContent() ?>
<br>
<br>
comment: <br>
<?php foreach (Comments::allComments($blog->getId()) as $value): ?>
    <br>
    Имя: <?= $value->getName() ?>
    <br>
    Комментарий: <?= $value->getBody() ?>
    <br>
<?php endforeach; ?>
<br>
Написать комментарий:
<br>
<form action="createComment.php" method="post">
    name:<input type="text" name="name"><br>
    body:<textarea name="body"></textarea><br>
    <input type="hidden" name="entry_id" value="<?= $blog->getId() ?>"><br>
    <button>push</button>
</form>
<a href="index.php">return</a>
</body>
</html>
