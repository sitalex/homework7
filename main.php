<?php
require_once 'classes/Blog.php';
$blog = Blog::all();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<form action="createBlog.php" method="post">
    title:
    <br>
    <input type="text" name="title">
    <br>
    intro:
    <br>
    <input type="text" name="intro">
    <br>
    content:
    <br>
    <input type="text" name="content">
    <br>
    <button>Отправить</button>
</form>
<table>
    <tr>
        <th>title</th>
        <th>intro</th>
        <th>content</th>
        <th></th>
        <th></th>
        <th></th>
    </tr>
    <?php foreach ($blog as $value): ?>
        <tr>
            <td>
                <?= $value->getTitle() ?>
            </td>
            <td>
                <?= $value->getIntro() ?>
            </td>
            <td>
                <?= $value->getContent() ?>
            </td>
            <td>
                <a href="details.php?id=<?= $value->getId() ?>">more</a>
            </td>
            <td>
                <a href="update.php?id=<?= $value->getId() ?>">update</a>
            </td>
            <td>
                <a href="destroy.php?id=<?= $value->getId() ?>">destroy</a>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
</body>
</html>
