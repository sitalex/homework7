<?php
require_once 'db/db.php';

try
{
    $sql = "CREATE TABLE entries (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    title VARCHAR(255) NOT NULL,
    intro VARCHAR(255) NOT NULL,
    content TEXT) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($sql);

    $sql = "CREATE TABLE comments (
    id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    body TEXT,
    entry_id INT,
    FOREIGN KEY(entry_id) REFERENCES entries(id) ON DELETE CASCADE) DEFAULT CHARACTER SET utf8 ENGINE=InnoDB";
    $pdo->exec($sql);
}
catch (Exception $ex)
{
    header('Location:main.php');
}
header('Location:main.php');
